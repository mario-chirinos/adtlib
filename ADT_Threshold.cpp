////////////////////////////////////////////////////////////////////////////////
//	ADT_Threshold.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	12 Apr 2008 -
//------------------------------------------------------------------------------
//	Image thresholding methods.
//	Notes:	
//		
////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstring>
#include "ADT_Histogram.h"

#include "ADT_Threshold.h"
using namespace std;
//------------------------------------------------------------------------------
int BiModHisTh(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels)
{
unsigned int i;
float sum1, tot1, sum2 ,tot2;
unsigned int th;
	/*if(false)//nChannels != 1 || nChannels != 3)
	{
       		cerr << "input is not a 1 channel image" << endl;
       	return EXIT_FAILURE;
       	}*/

	ADT_ImageHistogram htgm(data, width, height, nChannels);
	bool change = true;
        sum1=0;
        for(i = 0; i < 256; i++)
	{
                sum1+=i*htgm(i,0);
        }
	th = sum1/(width*height);
        
	do
	{
		sum1 = 0; tot1 = 0;
		sum2 = 0; tot2 = 0;

		for(i = 0; i < 256; i++)
		{
			if(i<th)
			{
				sum1+= i*htgm(i, 0);
				tot1+=htgm(i,0);
			}
			else
			{
				sum2+= i*htgm(i, 0);
				tot2+=htgm(i,0);
			}
		}
                unsigned int newTh;
                if (!((tot1==0) || (tot2==0)))
                        newTh = (unsigned int)(( sum1/tot1 + sum2/tot2  )/2);
                else
                        newTh=128;

                change = (th != newTh);
                th = newTh;
	}while(change);
	BinThreshold(data, width, height, nChannels, th);
return th;
}
//------------------------------------------------------------------------------
void BinThreshold(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned char th)
{
        unsigned int i = width*height;
	for(;i--;)//unsigned int i = 0; i<width*height; i++)
	{
		for(unsigned int c = 0; c < nChannels; c++)
			*data++=(*data < th ? 0 : 255);
	}
}
//------------------------------------------------------------------------------
void localBinThreshold(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned char kernel)
{
	int minVariation = 255/5;
	int* min = new int [nChannels];
	int* max =new int [nChannels];

	unsigned char* dataTemp=new unsigned char[width*height*nChannels];
        memcpy(dataTemp, data, width*height*nChannels);
        for (unsigned int y=0; y<height; ++y)
		for (unsigned int x=0; x<width; ++x)
		{
			int index = nChannels*(y*width+x);

			for(unsigned int c=0; c<nChannels; c++)
			{
				min[c] = 255;
				max[c] = 0;
			}

			for (int nYMatrix = -kernel; nYMatrix <= (int)kernel; nYMatrix++)
				for (int nXMatrix = -kernel; nXMatrix <= (int)kernel; nXMatrix++)
				{
					int YmasnYMatrix = y + nYMatrix;
					int XmasnXMatrix = x + nXMatrix;
					int indexMas = nChannels*(YmasnYMatrix*width+XmasnXMatrix);
					 //if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < nHeight) && (0 <= XmasnXMatrix) && (XmasnXMatrix < nWidth) && ( (Kernel+.5)*(Kernel+.5) ) >= (nXMatrix*nXMatrix + nYMatrix*nYMatrix))
					if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < (int)height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < (int)width))
					{
						for(unsigned int c=0; c<nChannels; c++)
						{
						  	if (dataTemp[indexMas+c] < min[c])
								min[c]=dataTemp[indexMas+c];
							else if (dataTemp[indexMas+c] > max[c])
								max[c]=dataTemp[indexMas+c];
						}
					}
				}
			for(unsigned int c=0; c<nChannels; c++)
			{
				int th = max[c]+min[c];
				th/=2;
				data[index+c]=(th > dataTemp[index+c] ? 255:0);				
			}
		}
	delete[] min;
	delete[] max;
	delete[] dataTemp;
}
//------------------------------------------------------------------------------




