////////////////////////////////////////////////////////////////////////////////
// ADT_Draw2D.h
// Mario Chirinos Colunga
// 2013-08-31
//------------------------------------------------------------------------------
//	2D primitives for images
// Notes:
//	
//------------------------------------------------------------------------------

#ifndef ADT_DRAW2D_H
#define ADT_DRAW2D_H

// your public header include
//------------------------------------------------------------------------------
#include "ADT_Image.h"
//------------------------------------------------------------------------------

// the declaration of your class...
//------------------------------------------------------------------------------
void ADT_Draw2D_square(ADT_Image &image, int x1, int y1, int x2, int y2, int lineWidth, unsigned char red, unsigned char green, unsigned char blue, float alpha);
void ADT_Draw2D_circle(ADT_Image &image, int cx, int cy, int radius, int lineWidth, unsigned char red, unsigned char green, unsigned char blue, float alpha);
void ADT_Draw2D_line(ADT_Image &image, int x1, int y1, int x2, int y2, int lineWidth, unsigned char red, unsigned char green, unsigned char blue, float alpha);
//------------------------------------------------------------------------------
#endif
