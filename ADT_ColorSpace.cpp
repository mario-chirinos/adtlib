//////////////////////////////////////////////////////////////////////////////
//	ADT_ColorSpace.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	12 Mar 2OO8 -
//----------------------------------------------------------------------------
//	Convention between color spaces
//	Notas:
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include"ADT_Image.h"

#include"ADT_ColorSpace.h"
using namespace std;
//----------------------------------------------------------------------------
int rgbToGray(ADT_Image &image)
{
	if(image.getChannels() != 3)
	{	
		cerr << "input is not a 3 channels image" << endl; 
		return 1;
	}
	ADT_Image temp(image.getWidth(), image.getHeight(), 1);
	unsigned char *data = image.data;
	unsigned char *dataTemp = temp.data;
	for (unsigned int i = 0; i<temp.getWidth()*temp.getHeight(); i++)
	{
		int sum = 0;
		for(unsigned int c = 0; c < image.getChannels(); c++)
			sum+=*data++;	
		*dataTemp++=sum/image.getChannels();
	}
	image=temp;
return 0;
}
//----------------------------------------------------------------------------
int grayToRgb(ADT_Image &image)
{
	if(image.getChannels() != 1)
	{	
		cerr << "input is not a One channel image" << endl; 
		return 1;
	}
	ADT_Image temp(image.getWidth(), image.getHeight(), 3);
	unsigned char *data = image.data;
	unsigned char *dataTemp = temp.data;
	for (unsigned int i = 0; i<temp.getWidth()*temp.getHeight(); i++)
	{
		*dataTemp++=data[i];
		*dataTemp++=data[i];
		*dataTemp++=data[i];
	}
	image=temp;
return 0;
}
//----------------------------------------------------------------------------
