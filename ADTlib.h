//////////////////////////////////////////////////////////////////////////////
//	ADTlib.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	Aurea - Desarrollo Tecnológico
//	28 May 07 - 28 May 07 - 03 Feb 08 - 16 Jun 09 - 24 jun 09 - 14 Ago 10
//----------------------------------------------------------------------------
//	Library
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////

#ifndef ADTLIB_H
#define ADTLIB_H

#include "ADT_DataTypes.h"
#include "ADT_Image.h"	// -libjasper
#include "ADT_Histogram.h"
#include "ADT_ColorSpace.h"
#include "ADT_Threshold.h"
#include "ADT_Filters.h"
#include "ADT_LinearAlgebra.h"	// -libblas
#include "ADT_Segmentation.h"
#include "ADT_VectorSort.h"
#include "ADT_Tools.h"
#include "ADT_Geometry.h"
#include "ADT_Draw2D.h"
//#include "ADT_GTK.h"		//pkg-config --cflags --libs gtk+-2.0 libgnomeui-2.0
//#include "ADT_GStreamer.h" //pkg-config --cflags gstreamer-0.10

//----------------------------------------------------------------------------

#endif // ADTLIB_H
// EOF
