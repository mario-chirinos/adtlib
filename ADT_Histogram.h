//////////////////////////////////////////////////////////////////////////////
//	ADT_Histogram.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	12 Apr 2008 - 
//----------------------------------------------------------------------------
//	Image histogram class and related functions.
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_HISTOGRAM_H
#define ADT_HISTOGRAM_H

// your public header include
//..
// the declaration of your class...
class ADT_ImageHistogram
{
 public:	void		get(unsigned char *data, unsigned int width_, unsigned int height_, unsigned int nChannels_);
   	unsigned int 	getHeight() const;
   	unsigned int 	getWidth() const;
	int 		save(const char *fileName);
	ADT_ImageHistogram(unsigned char *data_, unsigned int width_, unsigned int height_, unsigned int nChannels_);
	ADT_ImageHistogram();
	~ADT_ImageHistogram();
        //unsigned long int &operator ()(unsigned int i, unsigned int c);
	unsigned long int operator ()(unsigned int i, unsigned int c) const;
 private:
	unsigned int nChannels;
	unsigned int height, width;	
	unsigned long int *data;
	void clear();

};
#endif // ADT_HISTOGRAM_H
// EOF
