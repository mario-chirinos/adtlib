//////////////////////////////////////////////////////////////////////////////
//	ADT_Geometry.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	29 May 2007 - 24 Ago 2008
//----------------------------------------------------------------------------
//	Functions for prjective geometry
//	Notes:	
//		29/May/2007:	The code is currently using LAPACKpp 1) for 
//				linear algebra operations.
//				Other alternatives are ATLAS or Octave.
//	1) http://lapackpp.sourceforge.net/
//		24 Ago 2008
//		Modificar projectImgFit para que projecte la imagen
//		ajustada al tamaño
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <math.h>
#include <cstring>
//#include "lapackpp/lapackpp.h"
#include "ADT_DataTypes.h"
#include "ADT_LinearAlgebra.h"
#include "ADT_Geometry.h"
#include "ADT_Filters.h"
using namespace std;
//------------------------------------------------------------------------------
void scaleImage(ADT_Image &image, float scale)
{
	if(scale==1||scale<0)
		return;
	int height = image.getHeight()*scale;
	int width = image.getWidth()*scale;
	int nChannels = image.getChannels();
	cout << "scale=" << scale << " new size: " << width <<" x " << height << endl;
	ADT_Image temp(width, height, image.getChannels());
	
	if(scale <1)
	{
		gaussianBlur(image.data, image.getWidth(), image.getHeight(), image.getChannels(), 1/(2*scale));

		for (unsigned int y=0; y<height; y++)
		{
			for (unsigned int x=0; x<width; x++)
			{
				int index = nChannels*((int)(1/scale*(float)y)*image.getWidth()+(int)(x*1/scale));
				int index1 = nChannels*(y*width+x);
				for(unsigned int c=0; c<nChannels; c++)
				{
					temp.data[index1+c]=image.data[index+c];
				}
			}
		}
	}
	if(scale>1)
	{
		cerr << "image scale > 1 not implemented" << endl;
	}
	image=temp;
}
////------------------------------------------------------------------------------
////Bilinear interpolation
//unsigned char subPixelColor(const unsigned char *data, unsigned int nWidth,  unsigned int nHeight, unsigned int nChannels, double pX, double pY, unsigned int channel)
//{
//	unsigned char newColor = 0;
//	double intX, intY;
//	double fractionX = modf(pX, &intX); double fractionY = modf(pY, &intY);

//	int iX = (int)floor(pX); int iY = (int)floor(pY);
//	int iX2 = (int)ceil(pX); int iY2 = (int)ceil(pY);
//	double area, dx, dy;

//                for (int y= iY; y<=iY2; y++)
//                 for (int x= iX; x<=iX2; x++)
//                 {
//                        int Index = nChannels*(y*nWidth+x);
//                         dx = ( x == iX ? (1-fractionX) : fractionX);
//                          dy = ( y == iY ? (1-fractionY) : fractionY);

//                        area = dx*dy;
//                        if (iY2< (int)nHeight && iX2 < (int)nWidth)
//                          newColor += (unsigned char)(data[Index+channel]*area);
//                        else
//                          newColor = data[Index+channel];
//                 }
//                
//return newColor;
//}
////----------------------------------------------------------------------------
//ADT_Matrix get2dHomogrphyMatrix(const ADT_Matrix &xIn, const ADT_Matrix &yIn, const ADT_Matrix &xOut, const ADT_Matrix &yOut)
//{
//// P2= H*P1  //Homography    // P1 = Original Image
//// P2.x  = H00*p1.x + H01*P1.y + H02 / H20*P1.x + H21*P1.y + 1
//// P2.x  = H00*p1.x + H01*P1.y + H02 - H20*P1.x*P2.x - H21*P1.y*P2.x;
//// P2.x  = H00*p1.x + H01*P1.y + H02 + 0*H10 + 0*H11 + 0*H12 - H20*P1.x*P2.x - H21*P1.y*P2.x;
//// P2.x  = A*p1.x + B*P1.y + C + 0*D + 0*E + 0*F - G*P1.x*P2.x - H*P1.y*P2.x;
////-----------------
//// P2.y = H10.P1.x + H11.P1.y + H12 / H20.P1.x + H21.P1.y + 1
//// P2.y = H10*P1.x + H11*P1.y + H12 - H20*P1.x*P2.y - H21*P1.y*P2.y;
//// P2.y = 0*H00 + 0*H01 + 0*H02 + H10*P1.x + H11*P1.y + H12 - H20*P1.x*P2.y - H21*P1.y*P2.y;
//// P2.y = 0*A + 0*B + 0*C + D*P1.x + E*P1.y + F - G*P1.x*P2.y - H*P1.y*P2.y;
////-----------------
//ADT_Matrix A(8, 8);
//ADT_Matrix b(8,1);
////void LaLinearSolve  	(const ADT_Matrix &   A,ADT_Matrix &  X,const ADT_Matrix &  B)  	
// //Compute the solution to a real system of linear equations A*X=B

//  for (int n=0; n<4; n++)
//  {
//    A(n, 0)=xIn(n,0); A(n, 1)=yIn(n,0); A(n, 2)=1; A(n, 3)=0; A(n, 4)=0; A(n, 5)=0; A(n, 6)=-xIn(n,0)*xOut(n,0); A(n, 7)=-yIn(n,0)*xOut(n,0);
//     b(n,0)=xOut(n,0);

//    A(n+4, 0)=0; A(n+4, 1)=0; A(n+4, 2)=0; A(n+4, 3)=xIn(n,0); A(n+4, 4)=yIn(n,0); A(n+4, 5)=1; A(n+4, 6)=-xIn(n,0)*yOut(n,0); A(n+4, 7)=-yIn(n,0)*yOut(n,0);
//     b(n+4,0)=yOut(n,0);
//  }

//  ADT_Matrix H(3, 3);

// ADT_Matrix x = LinearSysSolve(A, b);

//  H(0, 0)=  x(0,0); H(0, 1)=  x(1,0); H(0, 2)=  x(2,0);
//  H(1, 0)=  x(3,0); H(1, 1)=  x(4,0); H(1, 2)=  x(5,0);
//  H(2, 0)=  x(6,0); H(2, 1)=  x(7,0); H(2, 2)=  1;

//return H;
//}
////----------------------------------------------------------------------------
//ADT_Matrix get2dAaffineMatrix(const ADT_Matrix &xIn, const ADT_Matrix &yIn, const ADT_Matrix &xOut, const ADT_Matrix &yOut)
//{
//// P2= H*P1  //Homography    // P1 = Original Image
//// P2.x  = H00*p1.x + H01*P1.y + H02
//// P2.x  = A*p1.x + B*P1.y + C + 0*D + 0*E + 0*F;
////-----------------
//// P2.y = H10.P1.x + H11.P1.y + H12
//// P2.y = 0*A + 0*B + 0*C + D*P1.x + E*P1.y + F;
////-----------------
//ADT_Matrix A(6, 6);
//ADT_Matrix b(6,1);
////void LaLinearSolve  	(const ADT_Matrix &   A,ADT_Matrix &  X,const ADT_Matrix &  B)  	
// //Compute the solution to a real system of linear equations A*X=B

//  for (int n=0; n<3; n++)
//  {
//    A(n, 0)=xIn(n,0); A(n, 1)=yIn(n,0); A(n, 2)=1; A(n, 3)=0; A(n, 4)=0; A(n, 5)=0;
//     b(n,0)=xOut(n,0);

//    A(n+3, 0)=0; A(n+3, 1)=0; A(n+3, 2)=0; A(n+3, 3)=xIn(n,0); A(n+3, 4)=yIn(n,0); A(n+3, 5)=1;
//     b(n+3,0)=yOut(n,0);
//  }

//ADT_Matrix H(3, 3);

//  ADT_Matrix x = LinearSysSolve(A, b);

//  H(0, 0)=  x(0,0); H(0, 1)=  x(1,0); H(0, 2)=  x(2,0);
//  H(1, 0)=  x(3,0); H(1, 1)=  x(4,0); H(1, 2)=  x(5,0);
//  H(2, 0)=  0;	  H(2, 1)=  0;    H(2, 2)=  1;

//return H;
//}
////-------------------imagen = H * imagen----------------------------------------
//void projectImg(unsigned char* image, unsigned int width, unsigned int height, unsigned int nChannels, ADT_Matrix &H)
//{
//unsigned char* imOut = new unsigned char[width * height * nChannels];
//	
//	for(unsigned int y=0; y<height; y++)
//	  for(unsigned int x=0; x<width; x++)
//	  {
//		int index = nChannels*(y*width+x);
//			ADT_Matrix p1(3,1), p2(3,1);
//			 int new_x, new_y;

//          		p1(0,0) =(double)x; p1(1,0) =(double)y; p1(2,0) =1;
//          		p2 = H*p1;				
//           		new_x = p2(0,0)/p2(2,0);
//            		 new_y = p2(1,0)/p2(2,0);

//		if((new_y>0)&&(new_x>0)&&(new_y<(int)height)&&(new_x<(int)width)) //checar  >0 y cambiar si se requiere a >=0
//		  for(unsigned int c=0; c< nChannels; c++)
//			imOut[index+c]= subPixelColor(image, width, height, nChannels, new_x, new_y, c);
//                else
//		  for(unsigned int c=0; c< nChannels; c++)
//			imOut[index+c]= 0;              
//	  }
//memcpy (image, imOut, width * height * nChannels);
//delete[] imOut;
//}
////-----------------------modificar------------------------------------
//void projectImgFit(unsigned char* dataIn, unsigned int iWidth, unsigned int iHeight, unsigned char* dataOut, unsigned int oWidth, unsigned int oHeight, unsigned int nChannels, ADT_Matrix &H)
//{
//	
//	for(unsigned int y=0; y<oHeight; y++)
//	  for(unsigned int x=0; x<oWidth; x++)
//	  {
//		int index = nChannels*(y*oWidth+x);
//			ADT_Matrix p1(3,0), p2(3,0);
//			 int new_x, new_y;;

//          		p1(0,0) =(double)x; p1(1,0) =(double)y; p1(2,0) =1;

//          		p2 = H*p1;				

//           		new_x = p2(0,0)/p2(2,0);
//            		 new_y = p2(1,0)/p2(2,0);

//		if((new_y>0)&&(new_x>0)&&(new_y<(int)iHeight)&&(new_x<(int)iWidth)) //checar  >0 y cambiar si se requiere a >=0
//		  for(unsigned int c=0; c< nChannels; c++)
//			dataOut[index+c]= subPixelColor(dataIn, iWidth, iHeight, nChannels, new_x, new_y, c);
//                else
//		  for(unsigned int c=0; c< nChannels; c++)
//			dataOut[index+c]= 0;              
//	  }
//}
////----------------------------------------------------------------------------
//// g++ -c ADT_Geometry.cpp -I /usr/local/include/ -I /usr/local/include/lapackpp
//// g++ -c ADT_Geometry.cpp -I /usr/local/include/lapackpp

