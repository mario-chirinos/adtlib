//////////////////////////////////////////////////////////////////////////////
//	ADT_Segmentation.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	02 May 2008 - 03 Jul 2009
//----------------------------------------------------------------------------
//	Image segmentation algorithms
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstring>
#include "ADT_Segmentation.h"
using namespace std;
//----------------------------------------------------------------------------
// Inputs:
// 	X,Y: search start location
// 	data, width, height: single channel image
// 	map: region map of the same size of input image
// 	kernel: radius of search
// 	nR: region number with which the map is going to be labeled
// Output: 
//	number of pixels in the region
vector <ADT_Point2D_ui> conectedComponent(unsigned int X, unsigned int Y, const unsigned char *data, unsigned int width, unsigned int height, unsigned int *map, unsigned int kernel, unsigned int nR)
{
 vector <ADT_Point2D_ui> pixchain;
 ADT_Point2D_ui pixTemp; 
 unsigned int index = (Y*width+X);
 const unsigned char color = data[index];

        pixTemp.x = X;
        pixTemp.y = Y;

	pixchain.clear();

        pixchain.push_back(pixTemp);
	unsigned int nMax = 1;
	unsigned int n = 0;
	do
        {
		X = pixchain[n].x;
		Y = pixchain[n].y;

		for (int nYMatrix = -(int)kernel; nYMatrix <= (int)kernel; nYMatrix++)
		{
			for (int nXMatrix = -(int)kernel; nXMatrix <= (int)kernel; nXMatrix++)
			{
                        	int YmasnYMatrix = Y + nYMatrix;
                        	int XmasnXMatrix = X + nXMatrix;
				int indexMas = (YmasnYMatrix*width+XmasnXMatrix);
                        	if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < (int) height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < (int)width) && ( ((kernel+.5)*(kernel+.5)) >= (nXMatrix*nXMatrix + nYMatrix*nYMatrix) )&& color==data[indexMas] && map[indexMas]==0)
				//if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < (int) height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < (int)width) && color==data[indexMas] && map[indexMas]==0)
				{
                                	nMax++;
                                	pixTemp.x = XmasnXMatrix;
                                	pixTemp.y = YmasnYMatrix;
                                	pixchain.push_back(pixTemp);
					map[indexMas]=nR; //nR must be != 0
                        	}
			}
		}
		n++;
	}while (nMax != n);

return pixchain;

}
//----------------------------------------------------------------------------
vector < vector <ADT_Point2D_ui> > ccSegmentation(const unsigned char *data, unsigned int width, unsigned int height, unsigned char color)
{
unsigned int *map = new unsigned int[width*height];
vector < vector <ADT_Point2D_ui> > segments;

	
	memset(map,0,width*height*sizeof(unsigned int));

	int nRegion = 0;
	
	for(unsigned int y = 0; y < height; y++)
	{
        	for(unsigned int x = 0; x < width; x++)
        	{
			unsigned int index = y*width+x;
                	if ( map[index] == 0  && data[index] == color)
                	{
	                        nRegion++;
				segments.push_back(conectedComponent(x, y, data, width, height, map, 1, nRegion));
			}
		}
	}

delete []map;
return segments;
}
//----------------------------------------------------------------------------
