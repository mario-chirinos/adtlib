//////////////////////////////////////////////////////////////////////////////
//	ADT_VectorSort.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	18 Apr 2008
//----------------------------------------------------------------------------
//	Vector sort and mediand find algorithms.
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_VECTORSORT_H
#define ADT_VECTORSORT_H

// your public header include
//..
// the declaration of your class...
typedef int elem_type;
//template <class elem_type>
elem_type quick_select(elem_type arr[], int n);

#endif // ADT_VECTORSORT_H
// EOF
