//////////////////////////////////////////////////////////////////////////////
//	ADT_ColorSpace.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	29 May 2007 - 29 May 2007
//----------------------------------------------------------------------------
//	Convention between color spaces
//	Notas:
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_COLORSPACE_H
#define ADT_COLORSPACE_H
int rgbToGray(ADT_Image &image);
int grayToRgb(ADT_Image &image);
#endif // ADT_COLORSPACE_H
// EOF
