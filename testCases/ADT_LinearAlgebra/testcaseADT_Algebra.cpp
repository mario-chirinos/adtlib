////////////////////////////////////////////////////////////////////////////////
//
//
//
////////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include "ADT_LinearAlgebra.h"
using namespace std;
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	if(argc < 0)
	{
		cerr << "too few arguments" << endl;
		return EXIT_FAILURE;
	}

 ADT_Matrix m(10,5,1);
	double v1[] = {	1,2,
			1,0,
			1,2};
	double v2[] = { 0,0,
			7,5,
			2,1};

 ADT_Matrix a(3,2,v1) ;
 ADT_Matrix b(3,2,v2);

	cout << "-----Matrix construtors-----" <<endl;
	cout << "ADT_Matrix m(10,5,1)" << endl;
	cout << m << endl;
	cout << "ADT_Matrix a(3,2,v1)\n" << a <<endl;
	cout << "ADT_Matrix b(3,2,v1)\n" << b <<endl;

	cout << "-----Matrix Overloaded Operators-----" << endl;
ADT_Matrix c;
	c=a;
	cout << "operator = , c = a\n" << c << endl;
	c+=b;
	cout << "operator += , c += b\n" << c << endl;
	c=a+b;
	cout << "operator + , c = a + b\n" << c << endl;
	c-=b;
	cout << "operator -= , c -= b\n" << c << endl;
	c=a-b;
	cout << "operator - , c -= a-b\n" << c << endl;

	c+=1;
	cout << "operator += , c += 1\n" << c << endl;
	c=a+1;
	cout << "operator + , c = a + 1\n" << c << endl;
	c-=1;
	cout << "operator -= , c -= 1\n" << c << endl;
	c=a-1;
	cout << "operator - , c -= a-1\n" << c << endl;
	double v3[] = {	0,7,2,
			0,5,1};
	ADT_Matrix d(2,3,v3);
	c=a*d;
	cout << "operator * , c = a*d\n" << a << "*\n" << d << "=\n" << c << endl;
	cout <<" c(0,1)= " << c(0,1) << " c(1,2)= " << c(1,2)<< endl;
	double v4[] = {0,7,2};
	d =ADT_Matrix(3,1,v4);
	cout << "c*d = \n" << c << " *\n" << d << "= \n" << c*d << endl;

	cout << "-----Funciones-----" << endl;
	cout << "transpuesta: \n" << a << "^T=\n"  << adtLA_transpose(a) << endl;
	cout << "norm 2: \n norm2(A)=\n" << adtLA_norm2(a) << endl;
	cout << "norm 2: \n norm2(d)=\n" << adtLA_norm2(d) << endl;
 return 0;
}
//------------------------------------------------------------------------------
