////////////////////////////////////////////////////////////////////////////////
//
//
//
////////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include "ADT_Segmentation.h"
#include "ADT_Image.h"
#include "ADT_ColorSpace.h"
using namespace std;
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	if(argc < 0)
	{
		cerr << "too few arguments" << endl;
		return EXIT_FAILURE;
	}

	ADT_Image image("test.pnm");
	cout << "width: " << image.getWidth() << " height: " << image.getHeight() << " channels: " << image.getChannels() << endl;
	rgbToGray(image);
	cout << "width: " << image.getWidth() << " height: " << image.getHeight() << " channels: " << image.getChannels() << endl;


	vector< vector<ADT_Point2D_ui> > segments;
	segments = ccSegmentation(image.data, image.getWidth(), image.getHeight(),0);
	cout << "count: " << segments.size() << endl;

	segments = ccSegmentation(image.data, image.getWidth(), image.getHeight(),0);
	cout << "count: " << segments.size() << endl;

	ADT_Image image2("test.pnm");
	rgbToGray(image2);
	vector< vector<ADT_Point2D_ui> > segments2;
	segments2 = ccSegmentation(image2.data, image2.getWidth(), image2.getHeight(),0);
	cout << "count: " << segments2.size() << endl;

	segments2 = ccSegmentation(image2.data, image2.getWidth(), image2.getHeight(),0);
	cout << "count: " << segments2.size() << endl;
 return 0;
}
//------------------------------------------------------------------------------
