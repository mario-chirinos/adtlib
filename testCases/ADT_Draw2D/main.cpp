////////////////////////////////////////////////////////////////////////////////
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "ADT_Image.h"
#include "ADT_Draw2D.h"
using namespace std;
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	if(argc < 0)
	{
		cerr << "too few arguments" << endl;
		return 0;
	}
	ADT_Image image(512,512,3);
	ADT_Draw2D_square(image, 100,100,200,200, 10, 0, 255, 0, 0.5);
	ADT_Draw2D_circle(image, 256, 256, 100, 10, 255, 0, 0, 0.5);
	ADT_Draw2D_line(image, 100,100,200,200, 10, 0, 0, 255, 0.5);
	image.writeToFile("output.bmp", "bmp");
 return 0;
}
//------------------------------------------------------------------------------
