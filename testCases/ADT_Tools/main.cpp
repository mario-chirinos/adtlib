////////////////////////////////////////////////////////////////////////////////
//
//
//
////////////////////////////////////////////////////////////////////////////////
//#include <cstdlib>
#include <iostream>
#include "ADT_Tools.h"
using namespace std;
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	if(argc < 0)
	{
		cerr << "too few arguments" << endl;
		return 1;
	}

	cout << "--------------ADT_Tools test cases--------------" << endl;
	cout << "char* engNotation(long double value, const char *units, int exp)" << endl;
	cout << "1234 m: " << engNotation(1234, "m", 1) << endl;
	cout << "1234 m²: " << engNotation(1234, "m", 2) << endl;
	cout << "1234 m^3: " << engNotation(1234, "m", 3) << endl;
	cout << "0.0001234 m: " << engNotation(0.0001234, "m", 1) << endl;
	cout << "0.0001234 m²: " << engNotation(0.0001234, "m", 2) << endl;
	cout << "0.0001234 m³: " << engNotation(0.0001234, "m", 3) << endl<<endl;

	cout << "char* engNotation(long double value, const char *units)" << endl;
	cout << "1234 m: " << engNotation(1234, "m") << endl;
	cout << "1234 m²: " << engNotation(1234, "m²") << endl;
	cout << "1234 m³: " << engNotation(1234, "m³") << endl;
	cout << "0.0001234 m: " << engNotation(0.0001234, "m") << endl;
	cout << "0.0001234 m²: " << engNotation(0.0001234, "m²") << endl;
	cout << "0.0001234 m³: " << engNotation(0.0001234, "m³") << endl<<endl;

	cout << "123456789 m: " << engNotation(123456789, "m") << endl;
	cout << "123456789 m²: " << engNotation(123456789, "m²") << endl;
	cout << "123456789 m³: " << engNotation(123456789, "m³") << endl;
	cout << "0.000000123456789 m: " << engNotation(0.000000123456789, "m") << endl;
	cout << "0.000000123456789 m²: " << engNotation(0.000000123456789, "m²") << endl;
	cout << "0.000000123456789 m³: " << engNotation(0.000000123456789, "m³") << endl<<endl;

	cout << "const char* sciNotation(long double value, const char *units)" << endl;
	cout << "1234 m: " << sciNotation(1234, "m") << endl;
	cout << "1234 m²: " << sciNotation(1234, "m²") << endl;
	cout << "1234 m³: " << sciNotation(1234, "m³") << endl;
	cout << "0.0000001234 m: " << sciNotation(0.0001234, "m") << endl;
	cout << "0.0001234 m²: " << sciNotation(0.0001234, "m²") << endl;
	cout << "0.0001234 m³: " << sciNotation(0.0001234, "m³") << endl<<endl;

	cout << "123456789 m: " << sciNotation(1234, "m") << endl;
	cout << "123456789 m²: " << sciNotation(1234, "m²") << endl;
	cout << "123456789 m³: " << sciNotation(1234, "m³") << endl;
	cout << "0.000000123456789 m: " << sciNotation(0.000000123456789, "m") << endl;
	cout << "0.000000123456789 m²: " << sciNotation(0.000000123456789, "m²") << endl;
	cout << "0.000000123456789 m³: " << sciNotation(0.000000123456789, "m³") << endl;
	
	cout <<"--------------------------------"<<endl;
	string userPath = "/media";
	if (checkPath(userPath.c_str() ))
		cout << "Directory " << userPath << " does exist" << endl; 
	else
		cout << "Directory " << userPath << " does not exist" << endl; 
	userPath = "/123456789";
	if (checkPath(userPath.c_str() ))
		cout << "Directory " << userPath << " does exist" << endl; 
	else
		cout << "Directory " << userPath << " does not exist" << endl; 
	
 return 0;
}
//------------------------------------------------------------------------------
