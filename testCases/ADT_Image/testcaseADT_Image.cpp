////////////////////////////////////////////////////////////////////////////////
//
//
//
////////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include "ADT_Image.h"
using namespace std;
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	if(argc < 0)
	{
		cerr << "too few arguments" << endl;
		return EXIT_FAILURE;
	}

	cout << "ADT_Image imagen1(\"test.jpg\");" <<endl;
	ADT_Image imagen1("test.jpg");
	cout << "imagen1.writeToFile(\"im.jpg\", \"jpg\");" << endl;
	imagen1.writeToFile("im1.jpg", "jpg");
	cout << "Width: " << imagen1.getWidth() << endl;
	cout << "Height: " << imagen1.getHeight() << endl;
	cout << "Channels: " << imagen1.getChannels() << endl<< endl;

	cout << "default constructor: ADT_Image imagen2;"<<  endl;
	ADT_Image imagen2;
	cout << "overloaded = operator: imagen2  = imagen1;" <<  endl;
	imagen2  = imagen1;
	imagen2.writeToFile("im2.jpg", "jpg");
	cout << "overloaded - operator: imagen2  -= imagen1;"<<  endl;
	imagen2 -= imagen1;
	imagen2.writeToFile("im3.jpg", "jpg");

	cout << "overloaded - operator: imagen2  - imagen1;"<<  endl;
	ADT_Image imagen3;
	imagen3 =imagen2 - imagen1;
	imagen3.writeToFile("im4.jpg", "jpg");
 return 0;
}
//------------------------------------------------------------------------------
