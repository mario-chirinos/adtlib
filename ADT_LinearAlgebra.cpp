////////////////////////////////////////////////////////////////////////////////
//	ADT_LinearAlgebra.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	09 Jun 2009 - 
//------------------------------------------------------------------------------
//	Linear algebra class and functions	
//	Notes: 
//		Using cblas and clapack
//		http://www.netlib.org/clapack/
//		tutorial on
//		http://seehuhn.de/pages/linear
//		some code taken form
//		http://www.parashift.com/c++-faq-lite/operator-overloading.html
////////////////////////////////////////////////////////////////////////////////
#include "ADT_LinearAlgebra.h"
#include <cstring>
#include <iostream>
#include <cblas.h>
#include <cstdlib>
#include <cmath>
using namespace std;
//------------------------------------------------------------------------------
ADT_Matrix::ADT_Matrix()
{
	data=NULL;
}
//------------------------------------------------------------------------------
ADT_Matrix::~ADT_Matrix()
{
	delete[] data;
}
//------------------------------------------------------------------------------
ADT_Matrix::ADT_Matrix(const ADT_Matrix &mat)
{
	data=NULL;
	copy(mat);
}
//------------------------------------------------------------------------------
ADT_Matrix::ADT_Matrix(unsigned int rows, unsigned int cols, double fill)
{
	nRows=rows;
	nColumns=cols;
	data = new double[nRows*nColumns];
	for(unsigned int i=0; i<nRows*nColumns;i++)
		data[i]=fill;
	//memset(data, fill, nRows*nColumns);
}
//------------------------------------------------------------------------------
ADT_Matrix::ADT_Matrix(unsigned int rows, unsigned int cols, double *vector)
{
	nRows=rows;
	nColumns=cols;
	data = new double[nRows*nColumns];
	for(unsigned int i=0; i < nRows*nColumns; i++)
		data[i] = vector[i];
}
//------------------------------------------------------------------------------
unsigned int ADT_Matrix::getRows() const
{
	return nRows;
}
//------------------------------------------------------------------------------
unsigned int ADT_Matrix::getCols() const
{
	return nColumns;
}
//------------------------------------------------------------------------------
const double* ADT_Matrix::getData() const
{
	double *ptr = data;
	return ptr;
}
//------------------------------------------------------------------------------
double& ADT_Matrix::operator() (unsigned int row, unsigned int col)
{
	if (row >= nRows || col >= nColumns)
	{
		cerr << "Matrix subscript out of bounds"<<endl;
		exit(1);
	}
	return data[nColumns*row + col];
}

//------------------------------------------------------------------------------
double ADT_Matrix::operator() (unsigned int row, unsigned int col) const
{
	if (row >= nRows || col >= nColumns)
	{
		cerr << "Matrix subscript out of bounds" <<endl;
		exit(1);
	}
	return data[nColumns*row + col];
}
//------------------------------------------------------------------------------
ADT_Matrix &ADT_Matrix::operator= (const ADT_Matrix &param)
{
	copy(param);
	return *this;
}
//------------------------------------------------------------------------------
ADT_Matrix &ADT_Matrix::operator+=(const ADT_Matrix &param)
{
	if( nRows !=  param.getRows() || nColumns != param.getCols())
	{
		cerr << "Matrices are not of the same dimension"<<endl;
		exit(1);
	}

	for(unsigned int i=0; i<nRows*nColumns; i++)
		data[i]+=param.data[i];
	return *this;
}
//------------------------------------------------------------------------------
ADT_Matrix ADT_Matrix::operator+ (const ADT_Matrix &param) const
{
 
	return ADT_Matrix(*this) += param;

}
//------------------------------------------------------------------------------
ADT_Matrix &ADT_Matrix::operator-=(const ADT_Matrix &param)
{
	if( nRows !=  param.getRows() || nColumns != param.getCols())
	{
		cerr << "Matrices are not of the same dimension"<<endl;
		exit(1);
	}

	for(unsigned int i=0; i<nRows*nColumns; i++)
		data[i]-=param.data[i];
	return *this;
}
//------------------------------------------------------------------------------
ADT_Matrix ADT_Matrix::operator-(const ADT_Matrix &param) const
{
 
	return ADT_Matrix(*this) -= param;

}
//------------------------------------------------------------------------------
ADT_Matrix &ADT_Matrix::operator+=(const double &k)
{
	if( nRows <1 || nColumns <1)
	{
		cerr << "Matrix is not initialized"<<endl;
		exit(1);
	}

	for(unsigned int i=0; i<nRows*nColumns; i++)
		data[i]+=k;
	return *this;
}
//------------------------------------------------------------------------------
ADT_Matrix ADT_Matrix::operator+ (const double &k) const
{
 
	return ADT_Matrix(*this) += k;

}
//------------------------------------------------------------------------------
/*ADT_Matrix operator+(const double &k, const ADT_Matrix &param) const
{
	return param + k;
}*/
//------------------------------------------------------------------------------
ADT_Matrix &ADT_Matrix::operator-=(const double &k)
{
	if( nRows <1 || nColumns <1)
	{
		cerr << "Matrix is not initialized"<<endl;
		exit(1);;
	}

	for(unsigned int i=0; i<nRows*nColumns; i++)
		data[i]-=k;
	return *this;
}
//------------------------------------------------------------------------------
ADT_Matrix ADT_Matrix::operator-(const double &k) const
{
 
	return ADT_Matrix(*this) -= k;

}
//------------------------------------------------------------------------------
/*ADT_Matrix operator-(const double &k, const ADT_Matrix &param) const
{
	return param - k;
}*/
//------------------------------------------------------------------------------
ADT_Matrix  ADT_Matrix::operator*(const ADT_Matrix &param) const
{
	if(nColumns != param.getRows())
	{
		cerr << "Matrices dimension do not agree"<<endl;
		exit(1);
	}	
	unsigned int m = nRows;
	unsigned int p = nColumns;
	unsigned int n = param.getCols();

	ADT_Matrix C(m,n,0.0);
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, p, 1.0, data, p, param.data, n, 0.0, C.data, n);

 return C;
}
//------------------------------------------------------------------------------
ADT_Matrix  ADT_Matrix::operator*=(const double &k) const
{

	for(unsigned int i=0; i<nRows*nColumns; i++)
		data[i]*=k;
	return *this;
}
//------------------------------------------------------------------------------
ADT_Matrix  ADT_Matrix::operator*(const double &k) const
{

	return ADT_Matrix(*this) *= k;
}
//------------------------------------------------------------------------------
ostream& operator<<(ostream &os, const ADT_Matrix &param)
{
	for(unsigned int j=0; j<param.getRows(); j++)
	{
		for(unsigned int i=0; i<param.getCols(); i++)
			os << param(j,i) << " ";
		os << endl;
	}
 return os;
}
//------------------------------------------------------------------------------
void ADT_Matrix::copy(const ADT_Matrix &i)
{

	nColumns = i.getCols();
	nRows = i.getRows();

	if(data != NULL)
	{
		cout << "deleting data" << endl;
	 	delete[] data;
	}

	data=new double[nColumns*nRows];
        memcpy(data, i.data, nColumns*nRows*sizeof(double));
}

//------------------------------------------------------------------------------
//			Functions
//------------------------------------------------------------------------------
ADT_Matrix adtLA_transpose(const ADT_Matrix &mat)
{
	ADT_Matrix temp(mat.getCols(), mat.getRows(),0.0);
	for(unsigned int j=0; j<mat.getRows(); j++)
		for(unsigned int i=0; i<mat.getCols(); i++)
			temp(i,j)=mat(j,i);
 return temp;
}
//------------------------------------------------------------------------------
double adtLA_norm2(const ADT_Matrix &mat)
{

	if(mat.getRows()==1 || mat.getCols()==1)//vector
	{ 
		const double *data = mat.getData();
		return cblas_dnrm2(mat.getRows()*mat.getCols(), data, 1);
	}		
	else
		cerr << "norm L2 for matrices not yet implemented" << endl;//matrix n2=max(svd(mat))
 return 0;
}
//------------------------------------------------------------------------------
double adtLA_dot(const ADT_Matrix &a, const ADT_Matrix &b)
{

	if( (a.getRows()==1 || a.getCols()==1) && (b.getRows()==1 || b.getCols()==1) && (a.getRows()*a.getCols() == b.getRows()*b.getCols() ))//vector
	{ 
		const double *dataA = a.getData();
		const double *dataB = b.getData();
		return cblas_ddot(a.getRows()*a.getCols(), dataA, 1, dataB, 1);
	}		
	else
		cerr << "intput is not a vector or vectors are not of same lenght" << endl;
 return 0;
}
//------------------------------------------------------------------------------
