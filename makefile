#Mario Chirinos Colunga
# Geometry is mising, have to be updated
#--------------------------------------
APPNAME = libADT
#Compiler:
	#ifdef linux
		CC=g++
		MAKELIB=ar rcs
		OPT= 
		LIBEXT=.a
	#endif

	#ifdef win32
		#CC=bcc32
		#MAKELIB=tlib /C
		#OPT=-+
		#LIBEXT= 
	#endif

#Compiler flags
	CFLAGS=-c -g -Wall -O3

#Directories
	libDIR= /usr/local/lib	
	incDIR= /usr/local/include
	lapackDIRinc= /usr/local/include/lapackpp
#Library flags
	INCLUDES=$(shell pkg-config --cflags gtk+-3.0  gstreamer-1.0)
#	libGTK=$(shell pkg-config --libs gtk+-2.0)libgnomeui-2.0
#--------------------------------------
all: Project

Project: ADT_LinearAlgebra.o ADT_Threshold.o ADT_Filters.o ADT_ColorSpace.o ADT_Histogram.o \
	ADT_Image.o ADT_Jasper.o ADT_DataTypes.o ADT_Segmentation.o ADT_VectorSort.o \
	ADT_GTK.o ADT_Tools.o ADT_GStreamer.o ADT_Geometry.o ADT_Draw2D.o
	$(MAKELIB) $(APPNAME)$(LIBEXT) \
	$(OPT)ADT_DataTypes.o \
	$(OPT)ADT_Jasper.o \
	$(OPT)ADT_Image.o \
	$(OPT)ADT_Histogram.o \
	$(OPT)ADT_ColorSpace.o \
	$(OPT)ADT_Threshold.o \
	$(OPT)ADT_Filters.o \
	$(OPT)ADT_LinearAlgebra.o \
	$(OPT)ADT_Segmentation.o \
	$(OPT)ADT_VectorSort.o \
	$(OPT)ADT_GTK.o \
	$(OPT)ADT_Tools.o \
	$(OPT)ADT_GStreamer.o \
	$(OPT)ADT_Geometry.o \
	$(OPT)ADT_Draw2D.o 

ADT_DataTypes.o: ADT_DataTypes.cpp
	$(CC) $(CFLAGS) ADT_DataTypes.cpp

ADT_Jasper.o: ADT_Jasper.cpp
	$(CC) $(CFLAGS) ADT_Jasper.cpp

ADT_Image.o: ADT_Image.cpp 
	$(CC) $(CFLAGS) ADT_Image.cpp

ADT_Histogram.o: ADT_Histogram.cpp
	$(CC) $(CFLAGS) ADT_Histogram.cpp

ADT_ColorSpace.o: ADT_ColorSpace.cpp
	$(CC) $(CFLAGS) ADT_ColorSpace.cpp

ADT_Threshold.o: ADT_Threshold.cpp
	$(CC) $(CFLAGS) ADT_Threshold.cpp

ADT_Filters.o: ADT_Filters.cpp
	$(CC) $(CFLAGS) ADT_Filters.cpp

ADT_LinearAlgebra.o: ADT_LinearAlgebra.cpp
	$(CC) $(CFLAGS) ADT_LinearAlgebra.cpp

ADT_Segmentation.o: ADT_Segmentation.cpp
	$(CC) $(CFLAGS) ADT_Segmentation.cpp

ADT_VectorSort.o: ADT_VectorSort.cpp
	$(CC) $(CFLAGS) ADT_VectorSort.cpp

ADT_GTK.o: ADT_GTK.cpp
	$(CC) $(CFLAGS) ADT_GTK.cpp $(INCLUDES)

ADT_Tools.o: ADT_Tools.cpp
	$(CC) $(CFLAGS) ADT_Tools.cpp $(INCLUDES)

ADT_GStreamer.o: ADT_GStreamer.cpp
	$(CC) $(INCLUDES) $(CFLAGS) \
	ADT_GStreamer.cpp 

ADT_Geometry: ADT_Geometry.cpp
	$(CC) $(INCLUDES) $(CFLAGS) \
	ADT_Geometry.cpp

ADT_Draw2D: ADT_Draw2D.cpp
	$(CC) $(INCLUDES) $(CFLAGS) \
	ADT_Draw2D.cpp
	
#ar rcs libfunc.a func1.o func2.o func3.o
#g++ -o exe_file exe_file.o -L. -lfunc

