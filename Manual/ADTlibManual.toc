\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{2}{section.1}
\contentsline {section}{\numberline {2}Manipulaci\IeC {\'o}n de im\IeC {\'a}genes}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}ADT\_Image}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Archivos de imagen: JasPer}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}ADT\_Jasper}{2}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Requisitos}{2}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Caso de prueba}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Interfaz gr\IeC {\'a}fica: GTK+ y Glade}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}ADT\_GTK}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Programa principal}{3}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}Requisitos}{3}{subsection.3.2}
\contentsline {section}{\numberline {4}Dispositivos de v\IeC {\'\i }deo: GStreamer}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}ADT\_Video}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}ADT\_GStreamer}{5}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Requisitos}{7}{subsubsection.4.2.1}
\contentsline {section}{\numberline {5}\IeC {\'A}lgebra lineal}{7}{section.5}
\contentsline {section}{\numberline {6}Procesamiento de im\IeC {\'a}genes}{7}{section.6}
\contentsline {subsection}{\numberline {6.1}ADT\_ColorSpace}{7}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}ADT\_Filters}{7}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}ADT\_Geometry}{7}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}ADT\_Histogram}{7}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}ADT\_Threshold}{7}{subsection.6.5}
\contentsline {section}{\numberline {7}Licencias de c\IeC {\'o}digo abierto}{7}{section.7}
\contentsline {subsection}{\numberline {7.1}MIT License}{7}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}BSD}{7}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}GPL}{8}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}LGPL}{8}{subsection.7.4}
