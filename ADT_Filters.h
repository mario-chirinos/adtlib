//////////////////////////////////////////////////////////////////////////////
//	ADT_Filters.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	31 May 2007 - 31 May 2007
//----------------------------------------------------------------------------
//	Image filter functions
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_FILTERS_H
#define ADT_FILTERS_H
void gaussianBlur(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned int kernel);
void SUSAN(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned int kernel, unsigned int th);
void median2D(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned int kernel);
unsigned char get2DKernelMedian(unsigned char *data, unsigned int &width, unsigned int &height, unsigned int &nChannels, unsigned int &kernel, unsigned int &x, unsigned int &y, unsigned int &channel);
#endif // ADT_DATATYPES_H
// EOF
