//////////////////////////////////////////////////////////////////////////////
//	ADT_Jasper.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	02 Feb 2008 - 02 Jul 2008
//----------------------------------------------------------------------------
//	Jasper[1] implementation 
//	[1] 	Copyright (c) 2001-2006 Michael David Adams
//		Copyright (c) 1999-2000 Image Power, Inc.
//		Copyright (c) 1999-2000 The University of British Columbia 
//		http://www.ece.uvic.ca/~mdadams/jasper/
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <jasper/jasper.h>

#include "ADT_Jasper.h" 
using namespace std;

bool ADT_jasper_openImage(unsigned char *&data, unsigned int &width, unsigned int &height, unsigned int &nChannels, const char *name)
{
jas_stream_t *imgStream;
jas_image_t *image;
jas_matrix_t *imgData;
int imgFmtId;
	if (jas_init()) 
	{
		cerr << "can not initialize jasper library" << endl;
		abort();
		return EXIT_FAILURE;
	}

	/* Open the image file. */
	if (!(imgStream = jas_stream_fopen(name, "rb")))
	{
		cerr << "cannot open:" << endl << name << endl;
		return false;
	}

	if ((imgFmtId = jas_image_getfmt(imgStream)) < 0)
	{
		cerr << "unknown image format" << endl;
		return false;
	}
	
	/* Decode the image. */
	if (!( image = jas_image_decode(imgStream, imgFmtId, 0)))
	{
		cerr << "cannot load image" << endl;
		return false;
	}

	/* Close the original image file. */
	jas_stream_close(imgStream);

	nChannels= jas_image_numcmpts(image);
	width = jas_image_cmptwidth(image, 0);
	height = jas_image_cmptheight(image, 0);
	int depth = jas_image_cmptprec(image, 0);	

	if (!(imgData = jas_matrix_create(height, width)))
	{
			cerr << "internal error: jas_matrix_create" << endl;
			return false;
	}

	data = new unsigned char [nChannels*width*height];

	for (unsigned int i=0; i<nChannels; i++)
	{		
		if (width != (unsigned int) jas_image_cmptwidth(image, i)
			|| height != (unsigned int) jas_image_cmptheight(image, i)
			|| depth != jas_image_cmptprec(image, i))
		{
			cerr << "image dimensions differ" << endl;
			return false;
		}		

		if (jas_image_readcmpt(image, i, 0, 0,  (jas_image_coord_t)width, (jas_image_coord_t)height, imgData))
		{
			cerr << "cannot read component data" << endl;
			return false;
		}

		//memcpy(data + i*width*height, (unsigned char *)imgData->data_, width*height);
		for (unsigned int y = 0; y < height; ++y)
		{
			for (unsigned int x = 0; x < width; ++x)
			{
				data[nChannels*(y*width+x)+i]= jas_matrix_get(imgData, y, x);	//rgbrgbrgb..
			}
		}
	}

	jas_matrix_destroy(imgData);
	jas_image_destroy(image);
	jas_image_clearfmts();
return true;
}
//---------------------------------------------------------------------------
bool ADT_jasper_saveImage(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, const char *name, const char *format)
{
	if (jas_init()) 
	{
		cerr << "can not initialize jasper library" << endl;
		abort();
		return false;
	}

int depth = 8;
jas_image_cmptparm_t compparms[nChannels];
jas_image_t *image;
jas_matrix_t *imgData[nChannels];

	for (unsigned int i = 0; i < nChannels; ++i)
	{
		compparms[i].tlx = 0;
		compparms[i].tly = 0;
		compparms[i].hstep = 1;
		compparms[i].vstep = 1;
		compparms[i].width = width;
		compparms[i].height = height;
		compparms[i].prec = depth;
		compparms[i].sgnd = false;
	}

	if (!(image = jas_image_create(nChannels, compparms, JAS_CLRSPC_UNKNOWN)))
	{	
		abort();		
	}

	switch (nChannels)
	{
	 case 1:
		jas_image_setclrspc(image, JAS_CLRSPC_SGRAY);
		jas_image_setcmpttype(image, 0,
		JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_GRAY_Y));
		break;
	case 3:	
		jas_image_setclrspc(image, JAS_CLRSPC_SRGB);
		jas_image_setcmpttype(image, 0,
		  JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_RGB_R));
		jas_image_setcmpttype(image, 1,
		  JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_RGB_G));
		jas_image_setcmpttype(image, 2,
		  JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_RGB_B));
		break;
	default:
		cerr << "Multispectral images are not supported" << endl;
		jas_image_destroy(image);
		return false;
	}


	for (unsigned int i = 0; i < nChannels; ++i)
	{
		if (!(imgData[i] = jas_matrix_create(height, width)))
		{
			cerr << "internal error" << endl;
			return false;
		}
	}

	for (unsigned int i = 0; i < nChannels; ++i)
	{
		//memcpy(imgData[i]->data_, (int*) data + i*width*height, width*height);	
		for (unsigned int y = 0; y < height; ++y)
		{
			for (unsigned int x = 0; x < width; ++x)
			{
				jas_matrix_set(imgData[i], y, x, data[nChannels*(y*width+x)+i]);	
			}
		}
	}

	for (unsigned int i = 0; i < nChannels; ++i)
	{
		if (jas_image_writecmpt(image, i, 0, 0, width, height, imgData[i]))
		{
			return false;
		}
	}

 jas_stream_t *outStream;
	if (!(outStream = jas_stream_fopen(name, "rwb")))
	{
		cerr << "cannot open diff stream" << endl;
		return false;
	}

	int fmtid = jas_image_strtofmt((char *)format);
	if (jas_image_encode(image, outStream, fmtid, 0))
	{
		cerr << "cannot save" << endl;
		return false;
	}

jas_stream_close(outStream);
jas_matrix_destroy(*imgData);
jas_image_destroy(image);
jas_image_clearfmts();
return true;
}

//---------------------------------------------------------------------------
void printImgInfo(jas_image_t *image)
{
	if (jas_init()) 
	{
		cerr << "can not initialize jasper library" << endl;
		abort();
		return;
	}
	cout << "jas_image_t info..." << endl;
	cout << "nChannels= " << jas_image_numcmpts(image) << endl;
	cout << "width = " << jas_image_cmptwidth(image, 0) << endl;
	cout << "height = " << jas_image_cmptheight(image, 0) << endl;
	cout << "depth = " << jas_image_cmptprec(image, 0) << endl;	
}
