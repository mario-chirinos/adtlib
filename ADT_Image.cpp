//////////////////////////////////////////////////////////////////////////////
//	ADT_Image.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	25 May 2007 - 2 Jul 2009
//----------------------------------------------------------------------------
//	Base calss for image manipulation
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "ADT_Image.h"
#include "ADT_Jasper.h"
using namespace std;
//---------------------------------------------------------------------------
ADT_Image::ADT_Image()
{
	data=NULL;	
	width=0;
	height=0;
	nChannels=0;
	init = false;
}
//---------------------------------------------------------------------------
ADT_Image::ADT_Image(unsigned int width_, unsigned int height_, unsigned int nChannels_)
{  	
	width=width_;
	height=height_;
	nChannels=nChannels_;
	//delete[] data;
	data= new unsigned char [width*height*nChannels];
	init = true;
}
//---------------------------------------------------------------------------
ADT_Image::ADT_Image(unsigned char* data_, unsigned int width_, unsigned int height_, unsigned int nChannels_)
{  	
	width=width_;
	height=height_;
	nChannels=nChannels_;
//	if(data != NULL)
//		delete[] data;
	data= new unsigned char [width*height*nChannels];
	memcpy (data, data_, width*height*nChannels);
	init = true;
}
//---------------------------------------------------------------------------
ADT_Image::ADT_Image(const char *fileName)
{

	ADT_jasper_openImage(data, width, height, nChannels, fileName);

}
//---------------------------------------------------------------------------
ADT_Image::ADT_Image(const ADT_Image &image)
{
	data=NULL;	
	copy(image);
}
//---------------------------------------------------------------------------
ADT_Image::~ADT_Image()
{
	delete[] data;
}
//---------------------------------------------------------------------------
ADT_Image &ADT_Image::operator= (const ADT_Image &param)
{
	copy(param);
	return *this;
}
//---------------------------------------------------------------------------
ADT_Image &ADT_Image::operator-=(ADT_Image &param)
{
	imAbsDif(data, param.data, width, height, nChannels);
	return *this;
}
//---------------------------------------------------------------------------
ADT_Image ADT_Image::operator- (ADT_Image &param) const
{
 
	if( nChannels !=  param.getChannels() || width != param.getWidth() || height != param.getHeight() )
	{
		cerr << "Images are not of the same dimension"<<endl;
		return NULL;
	}

return ADT_Image(*this) -= param;

}
//------------------------------------------------------------------------------
int ADT_Image::operator() (unsigned int x, unsigned int y, unsigned int channel) const
{
	if (y >= height || x >= width)
	{
		cerr << "Image subscript out of bounds" <<endl;
		exit(1);
	}
	
	return data[nChannels*(width*y + x) + channel];
}
//---------------------------------------------------------------------------
unsigned int ADT_Image::getWidth() const
{
	return width;
}
//---------------------------------------------------------------------------
unsigned int ADT_Image::getHeight() const
{
	return height;
}
//---------------------------------------------------------------------------
unsigned int ADT_Image::getChannels() const
{
	return nChannels;
}
//---------------------------------------------------------------------------
bool ADT_Image::writeToFile(const char* fileName, const char* format)
{
	ADT_jasper_saveImage(data, width, height, nChannels, fileName, format);
	cout << "Image file: " << fileName << " saved\n" << width << " x " << height << " pixels with " << nChannels << " channels\n\n"; 
return 0;
}
//---------------------------------------------------------------------------
void ADT_Image::copy(const ADT_Image &i)
{

	width = i.getWidth();
	height = i.getHeight();
	nChannels = i.getChannels();
	//init = i->initialized();
	if(data != NULL)
	{
		cout << "deleting data" << endl;
	 	delete[] data;
	}
	data=new unsigned char[width*height*nChannels];
        memcpy(data, i.data, width*height*nChannels);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void imAbsDif(unsigned char *data, unsigned char *dataIn, unsigned int width, unsigned int height, unsigned int nChannels)
{
	for(unsigned int i = 0; i < width*height*nChannels; i++)
	{
		unsigned char *data1 = data;
		*data++ = (unsigned char)abs((int)*data1++ - (int)*dataIn++);
	}
}
//---------------------------------------------------------------------------
// g++ -c ADT_Image.cpp -I /usr/local/include/opencv
