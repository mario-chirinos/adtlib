////////////////////////////////////////////////////////////////////////////////
//	ADT_LinearAlgebra.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	09 Jun 2009 - 
//------------------------------------------------------------------------------
//	Linear algebra class and functions	
//	Notes: 
//		Using cblas and clapack
//		http://www.netlib.org/clapack/
//		tutorial on
//		http://seehuhn.de/pages/linear
//		some code taken form
//		http://www.parashift.com/c++-faq-lite/operator-overloading.html
////////////////////////////////////////////////////////////////////////////////

#ifndef ADT_LINEARALGEBRA_H
#define ADT_LINEARALGEBRA_H
#include <iostream>
using namespace std;
// your public header include
//------------------------------------------------------------------------------
class ADT_Matrix
{
 public:
	
	ADT_Matrix();
	ADT_Matrix(const ADT_Matrix &mat);
	ADT_Matrix(unsigned int rows, unsigned int cols, double fill);
	ADT_Matrix(unsigned int rows, unsigned int cols, double *vector);
	~ADT_Matrix();

   	unsigned int getRows() const;
   	unsigned int getCols() const;
	const double* getData() const;
	void copy(const ADT_Matrix &i);

	double& operator() (unsigned int row, unsigned int col);
	double  operator() (unsigned int row, unsigned int col) const;

	ADT_Matrix &operator = (const ADT_Matrix &param);
	ADT_Matrix &operator+=(const ADT_Matrix &param);
	ADT_Matrix operator+(const ADT_Matrix &param) const;
	ADT_Matrix &operator-=(const ADT_Matrix &param);
	ADT_Matrix operator-(const ADT_Matrix &param) const;

	ADT_Matrix &operator+=(const double &k);
	ADT_Matrix operator+(const double &k) const;
	//ADT_Matrix operator+(const double &k, const ADT_Matrix &param) const;
	ADT_Matrix &operator-=(const double &k);
	ADT_Matrix operator-(const double &k) const;
	//ADT_Matrix operator-(const double &k, const ADT_Matrix &param) const;

	ADT_Matrix operator*(const ADT_Matrix &param) const;
	ADT_Matrix operator*=(const double &k) const;
	ADT_Matrix operator*(const double &k) const;

	friend ostream& operator<<(ostream &os, const ADT_Matrix &param); 

 private:
	unsigned int nRows;
	unsigned int nColumns;
	double *data;
	
}; 
//------------------------------------------------------------------------------
ADT_Matrix adtLA_transpose(const ADT_Matrix &mat);
double adtLA_norm2(const ADT_Matrix &mat);
double adtLA_dot(const ADT_Matrix &a, const ADT_Matrix &b);
#endif // ADT_LINEARALGEBRA_H
// EOF
