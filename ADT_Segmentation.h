//////////////////////////////////////////////////////////////////////////////
//	ADT_Segmentation.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	02 May 2008 -
//----------------------------------------------------------------------------
//	Image segmentation algorithms
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_SEGMENTATION_H
#define ADT_SEGMENTATION_H

#include <vector>
#include "ADT_DataTypes.h"
// your public header include
//..
// the declaration of your class...
using namespace std;
//----------------------------------------------------------------------------
vector <ADT_Point2D_ui> conectedComponent(unsigned int X, unsigned int Y, const unsigned char *data, unsigned int width, unsigned int height,unsigned int *map, unsigned int kernel, unsigned int nR);
vector < vector <ADT_Point2D_ui> > ccSegmentation(const unsigned char *data, unsigned int width, unsigned int height, unsigned char color);
#endif // ADT_SEGMENTATION_H
// EOF

