////////////////////////////////////////////////////////////////////////////////
// ADT_Draw2D.cpp
// Mario Chirinos Colunga
// 2013-08-31
//------------------------------------------------------------------------------
//	2D primitives for images
// Notes:
//	
//------------------------------------------------------------------------------
#include "ADT_Draw2D.h"
#include <cmath>
using namespace std;
//------------------------------------------------------------------------------
void ADT_Draw2D_square(ADT_Image &image, int x1, int y1, int x2, int y2, int lineWidth, unsigned char red, unsigned char green, unsigned char blue, float alpha)
{
	int width = image.getWidth();
	int height = image.getHeight();
	unsigned int nChannels = image.getChannels();
	unsigned char color[3] = {red, green, blue};
	for (int y=y1-lineWidth/2; y<=y2+lineWidth/2; y++)
	{
		for (int x=y1-lineWidth/2; x<=x2+lineWidth/2; x++)
		{
			int index = nChannels*(y*width+x);
			if(x>0 and y>0 and x<width and y<height and ( abs(float(y-y1))<=lineWidth/2  or abs(float(y-y2))<=lineWidth/2 or abs(float(x-x1))<=lineWidth/2  or abs(float(x-x2))<=lineWidth/2 ) )
			{
				for(unsigned int c=0; c<nChannels; c++)
				{
					image.data[index+c] = color[c]*alpha + image.data[index+c]*(1-alpha);
				}
			}

		}
	}
}
//------------------------------------------------------------------------------
void ADT_Draw2D_circle(ADT_Image &image, int cx, int cy, int radius, int lineWidth, unsigned char red, unsigned char green, unsigned char blue, float alpha)
{
	int width = image.getWidth();
	int height = image.getHeight();
	unsigned int nChannels = image.getChannels();
	unsigned char color[3] = {red, green, blue};
	for (int y=cy-radius-lineWidth/2; y<=cy+radius+lineWidth/2; y++)
	{
		for (int x=cx-radius-lineWidth/2; x<=cx+radius+lineWidth/2; x++)
		{
			int index = nChannels*(y*width+x);
			int r = sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy));
			if(x>0 and y>0 and x<width and y<height and r<=radius+lineWidth/2 and r>=radius-lineWidth/2)
			{
				for(unsigned int c=0; c<nChannels; c++)
				{
					image.data[index+c] = color[c]*alpha + image.data[index+c]*(1-alpha);
				}
			}

		}
	}
}
//------------------------------------------------------------------------------
void ADT_Draw2D_line(ADT_Image &image, int x1, int y1, int x2, int y2, int lineWidth, unsigned char red, unsigned char green, unsigned char blue, float alpha)
{
	int width = image.getWidth();
	int height = image.getHeight();
	unsigned int nChannels = image.getChannels();
	unsigned char color[3] = {red, green, blue};
	int minx = (x1<x2?x1:x2);
	int miny = (y1<y2?y1:y2);
	int maxx = (x1>x2?x1:x2);
	int maxy = (y1>y2?y1:y2);
	for(int x=minx; x<=maxx; x++)
	{
		float m = ( x2-x1==0 ? 0:(y2-y1)/(x2-x1) );					
		int y  = m*(x - x1) + y1;
		int index = nChannels*(y*width+x);
		if(x>0 and y>0 and x<width and y<height)
		{
			for(unsigned int c=0; c<nChannels; c++)
			{
				image.data[index+c] = color[c]*alpha + image.data[index+c]*(1-alpha);
			}
		}
	}
}
//------------------------------------------------------------------------------
