//////////////////////////////////////////////////////////////////////////////
//	ADT_Filters.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	31 May 2007 - 14 Apr 2008
//----------------------------------------------------------------------------
//	Image filter functions
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cmath>
#include <vector>
#include <cstring>
#include <cstdlib>
#include "ADT_Filters.h"
using namespace std;
//------------------------------------------------------------------------------
void gaussianBlur(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned int kernel)
{
	float* sum =new float[nChannels];
	
	unsigned char* dataTemp=new unsigned char[width*height*nChannels];
	memcpy(dataTemp, data, width*height*nChannels);

	for (unsigned int y=0; y<height; y++)
	{
		for (unsigned int x=0; x<width; x++)
		{
			int index = nChannels*(y*width+x);
			int n=0;
			for(unsigned int c=0; c<nChannels; c++)
			{
				sum[c] = 0;
			}
			for (int nYMatrix = -kernel; nYMatrix <= (int)kernel; nYMatrix++)
			{
				for (int nXMatrix = -kernel; nXMatrix <= (int)kernel; nXMatrix++)
				{
					int YmasnYMatrix = y + nYMatrix;
					int XmasnXMatrix = x + nXMatrix;
					int indexMas = nChannels*(YmasnYMatrix*width+XmasnXMatrix);
					if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < width) && ( (kernel+.5)*(kernel+.5) ) >= (nXMatrix*nXMatrix + nYMatrix*nYMatrix))
					//if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < (int)height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < (int)width))
					{
						for(unsigned int c=0; c<nChannels; c++)
						{
							sum[c]+=dataTemp[indexMas+c];
						}
						n++;                                          
					}
				}
			}
			for(unsigned int c=0; c<nChannels; c++)
			{
//				cout << sum[c]/n << endl;
				data[index+c]=sum[c]/n;
			}
		}
	}
	delete[] dataTemp;
	delete[] sum;
}
//------------------------------------------------------------------------------
void SUSAN(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned int kernel, unsigned int th)
{
	int* cont = new int [nChannels];
	int* diference =new int [nChannels];
	unsigned char* pixC = new unsigned char[nChannels];

		unsigned char* dataTemp=new unsigned char[width*height*nChannels];
		memcpy(dataTemp, data, width*height*nChannels);

		for (unsigned int y=0; y<height; y++)
		{
			for (unsigned int x=0; x<width; x++)
			{
				int index = nChannels*(y*width+x);
				int n=0;

				for(unsigned int c=0; c<nChannels; c++)
				{
					cont[c] = 0;
					pixC[c] = dataTemp[index+c];
				}

				for (int nYMatrix = -kernel; nYMatrix <= (int)kernel; nYMatrix++)
				{
					for (int nXMatrix = -kernel; nXMatrix <= (int)kernel; nXMatrix++)
					{
						int YmasnYMatrix = y + nYMatrix;
						int XmasnXMatrix = x + nXMatrix;
						int indexMas = nChannels*(YmasnYMatrix*width+XmasnXMatrix);
						if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < width) && ( (kernel+.5)*(kernel+.5) ) >= (nXMatrix*nXMatrix + nYMatrix*nYMatrix))
						//if ( (0 <= YmasnYMatrix) && (YmasnYMatrix < (int)height) && (0 <= XmasnXMatrix) && (XmasnXMatrix < (int)width))
						{
							for(unsigned int c=0; c<nChannels; c++)
							{
								diference[c] = abs(dataTemp[indexMas+c] - pixC[c]);
							  	if (diference[c] <= (int)th)
									cont[c]++;
							}
							n++;                                          
						}
					}
				}
				int ThG=3*n/4;
				for(unsigned int c=0; c<nChannels; c++)
					data[index+c] =(255/(float)ThG)*(cont[c]< ThG ? ThG-cont[c]:0);
			}
		}
	delete[] cont;
	delete[] diference;
	delete[] pixC;
	delete[] dataTemp;
}
//----------------------------------------------------------------------------
void median2D(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned int kernel)
{
	unsigned char *temp = new unsigned char[width*height*nChannels];
        memcpy(temp,(unsigned char*)data, width*height*nChannels);

	for (unsigned int y = 0; y < height; ++y)
        {
               	for (unsigned int x = 0; x < width; ++x)
               	{
			unsigned int index = nChannels*(y*width+x);
			for(unsigned int c = 0; c < nChannels; c++)
			{
				unsigned char median = get2DKernelMedian(temp, width, height, nChannels, kernel, x, y, c);
				data[index+c] = median;
			}
		}
	}
delete[] temp;
}
int compare (const void * a, const void * b)
{
  return ( *(unsigned char*)a - *(unsigned char*)b );
}
//----------------------------------------------------------------------------
unsigned char get2DKernelMedian(unsigned char *data, unsigned int &width, unsigned int &height, unsigned int &nChannels, unsigned int &kernel, unsigned int &x, unsigned int &y, unsigned int &channel)
{
	//vector <unsigned char> list;//1
	unsigned char *list = new unsigned char[(2*kernel+1)*(2*kernel+1)];//2
	unsigned int n = 0;
 	for (int yK = -(int)kernel; yK <= (int)kernel; yK++)
        {
		//unsigned char *list_=list;		
		double r = (2*(double)kernel+1)/2;
		unsigned int rxk = (unsigned int)floor(sqrt(r*r-yK*yK)); //correct circle shape 16/04/08
	
                for (int xK = - (int)rxk; xK <= (int)rxk; xK++)//(int xK = -(int)kernel; xK <= (int)kernel; xK++)//
                {
			if ( (0 <= y+yK) && (y+yK < height) && (0 <= x+xK) && (x+xK < width))
			{
				unsigned int indexK = nChannels*((y+yK)*width+(x+xK));
				list[n]= data[indexK+channel];//2
				n++;
				//list.push_back(data[indexK+channel]);//1
			}
		}
	}
	//sort (list.begin(), list.end());//1
qsort (list, n, sizeof(unsigned char), compare);//2
unsigned char median = list[n/2];
//cout << (int)median << endl;
//for(int i = 0; i<n; i++)
//	cout << (int)list[i]<< ", ";
//cout << n << endl;
//cin.get();
//delete[] list;//2
return median;
}
//----------------------------------------------------------------------------
