//////////////////////////////////////////////////////////////////////////////
//	ADT_VectorSort.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	18 Apr 2008
//----------------------------------------------------------------------------
//	Vector sort and mediand find algorithms.
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "ADT_VectorSort.h"
using namespace std;
//----------------Function that compares two elements-------------------------
/*
 to be used with qsort
 The function shall follow this prototype:
 int comparator ( const void * elem1, const void * elem2 );
 The function must accept two parameters that are pointers to elements, type-casted as void*.
 These parameters should be cast back to some data type and be compared.
 The return value of this function should represent whether elem1 is considered less than, equal,
 or grater than elem2 by returning, respectivelly, a negative value, zero or a positive value.
*/
int compareChar(const void * a, const void * b) 
{
  return ( *(unsigned char*)a - *(unsigned char*)b );	
}
//----------------------------------------------------------------------------
/*
* This Quickselect routine is based on the algorithm described in
* "Numerical recipes in C", Second Edition,
* Cambridge University Press, 1992, Section 8.5, ISBN 0-521-43108-5
* This code by Nicolas Devillard - 1998. Public domain.
*/

#define ELEM_SWAP(a,b) { register elem_type t=(a);(a)=(b);(b)=t; }
//template <class elem_type> 
elem_type quick_select(elem_type arr[], int n)
{
	unsigned char low, high ;
	unsigned char median;
	unsigned char middle, ll, hh;
	low = 0; high = n-1; median = (low + high) / 2;
	for (;;)
	{
		if (high <= low)	/* One element only */
			return arr[median] ;
		if (high == low + 1)	/* Two elements only */
		{ 
			if (arr[low] > arr[high])
				ELEM_SWAP(arr[low], arr[high]) ;
				return arr[median] ;
		}
		/* Find median of low, middle and high items; swap into position low */
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])
			ELEM_SWAP(arr[middle], arr[high]);
		if (arr[low] > arr[high])
			ELEM_SWAP(arr[low], arr[high]);
		if (arr[middle] > arr[low])
			ELEM_SWAP(arr[middle], arr[low]);

		/* Swap low item (now in position middle) into position (low+1) */
		ELEM_SWAP(arr[middle], arr[low+1]) ;

		/* Nibble from each end towards middle, swapping items when stuck */
		ll = low + 1;
		hh = high;
		for (;;)
		{
			do 
				ll++;
			while (arr[low] > arr[ll]);

			do
				hh--;
			while (arr[hh] > arr[low]);
			if (hh < ll)
				break;
			ELEM_SWAP(arr[ll], arr[hh]);
		}

		/* Swap middle item (in position low) back into correct position */
		ELEM_SWAP(arr[low], arr[hh]);

		/* Re-set active partition */
		if (hh <= median)
			low = ll;
		if (hh >= median)
			high = hh - 1;
	}
}
#undef ELEM_SWAP
//----------------------------------------------------------------------------
