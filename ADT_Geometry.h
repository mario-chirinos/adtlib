//////////////////////////////////////////////////////////////////////////////
//	ADT_Geometry.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	29 May 2007 - 24 Ago 2008
//----------------------------------------------------------------------------
//	Functions for prjective geometry
//	Notes:	
//		29/May/2007:	The code is currently using LAPACKpp [1] for 
//				linear algebra operations.
//				Other alternatives are ATLAS or Octave.
//	[1] http://lapackpp.sourceforge.net/
//////////////////////////////////////////////////////////////////////////////

#ifndef ADT_GEOMETRY_H
#define ADT_GEOMETRY_H
#include "ADT_Image.h"
// your public header include
//#include "lapackpp/lapackpp.h"
// nonmember functions
//----------------------------------------------------------------------------
void scaleImage(ADT_Image &image, float scale);
//unsigned char subPixelColor(const unsigned char *data, unsigned int nWidth, unsigned int nChannels, unsigned int nHeight, double pX, double pY, unsigned int channel);
//ADT_Matrix get2dHomogrphyMatrix(const ADT_Matrix &xIn, const ADT_Matrix &yIn, const ADT_Matrix &xOut, const ADT_Matrix &yOut);
//ADT_Matrix get2dAaffineMatrix(const ADT_Matrix &xIn, const ADT_Matrix &yIn, const ADT_Matrix &xOut, const ADT_Matrix &yOut);
//void projectImgFit(unsigned char* dataIn, unsigned int iWidth, unsigned int iHeight, unsigned char* dataOut, unsigned int oWidth, unsigned int oHeight, unsigned int nChannels, ADT_Matrix &H);
//void projectImg(unsigned char* image, const unsigned int width, const unsigned int height, unsigned int nChannels, ADT_Matrix &H);

#endif
//g++ -c ADT_Geometry.cpp -I /usr/local/include/lapackpp
