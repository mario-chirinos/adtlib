//////////////////////////////////////////////////////////////////////////////
//	ADT_Threshold.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	12 Apr 2008 - 
//----------------------------------------------------------------------------
//	Image thresholding methods
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_THRESHOLD_H
#define ADT_THRESHOLD_H

// your public header include
//..
// the declaration of your class...
int BiModHisTh(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels);
void BinThreshold(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned char th);
void localBinThreshold(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, unsigned char kernel);
#endif // ADT_THRESHOLD_H
// EOF
