//////////////////////////////////////////////////////////////////////////////
//	ADT_Histogram.cpp
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	12 Apr 2008 -
//----------------------------------------------------------------------------
//	Image histogram class and related functions.
//	Notes:	
//		
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <cstring>
#include "ADT_Histogram.h"
using namespace std;
//----------------------------------------------------------------------------
ADT_ImageHistogram::ADT_ImageHistogram()
{
	clear();
}
//----------------------------------------------------------------------------
ADT_ImageHistogram::ADT_ImageHistogram(unsigned char *data_, unsigned int width_, unsigned int height_, unsigned int nChannels_)
{
	get(data_, width_, height_, nChannels_);
}
//----------------------------------------------------------------------------
ADT_ImageHistogram::~ADT_ImageHistogram()
{
	clear();
}
//----------------------------------------------------------------------------
//unsigned long int &ADT_ImageHistogram::operator ()(unsigned int i, unsigned int c)
unsigned long int ADT_ImageHistogram::operator ()(unsigned int i, unsigned int c) const
{
	return data[i+256*c];
}
//----------------------------------------------------------------------------
void ADT_ImageHistogram::clear()
{
 /*	width =  0;
        height = 0;
	nChannels = 0;
	if(data!=NULL)
		delete[] data; 
*/
}
//----------------------------------------------------------------------------
void ADT_ImageHistogram::get(unsigned char *data_, unsigned int width_, unsigned int height_, unsigned int nChannels_)
{
clear();
        width =  width_;
        height = height_;
	nChannels = nChannels_;
        
        data = new long unsigned int[256*nChannels];
	memset(data, 0, sizeof (long unsigned int) *(256*nChannels));
        unsigned int i = width*height;
        for(; i--;)//unsigned int i = 0; i < width*height; i++) //
        {
		for(unsigned int c = 0; c < nChannels; c++, data_++)
		{
	       		//data[data_[i*nChannels+c] +256*c]++;
                        data[*data_ +256*c]++;
		}
	}

};
//---------------------------------------------------------------------------
unsigned int ADT_ImageHistogram::getWidth() const
{
	return width;
}
//---------------------------------------------------------------------------
unsigned int ADT_ImageHistogram::getHeight() const
{
	return height;
}
//---------------------------------------------------------------------------
int ADT_ImageHistogram::save(const char *fileName)
{
	unsigned long int *data_ = data;	
	ofstream file(fileName);
 	if (!file.is_open())
	{
		cerr << "Unable to open file" << endl;
		return 1;		
	}

	for(unsigned int c = 0; c < nChannels; c++)
	{
		file << "Channel: "<< c << endl;
		for(unsigned int i = 0; i < 256; i++)
		{
			file << i << ": " << *data_++<< endl;
		}
	}
  	file.close();
  	return 0;
}
//---------------------------------------------------------------------------
using namespace std;

